"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
    var randomNumber;
    return randomNumber = Math.floor(Math.random() * max) + min;
}

// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    return Math.round(number * 100) / 100;
    
}

// TODO Write a function which calculates and returns the volume of a cone.

function getVolume(height, radius){
    var volume = Math.PI * (radius * radius) * (height / 3);
    return roundTo2dp(volume);

}
// TODO Write a function which calculates and returns the volume of a cylinder.
function getCylinderVolume(height, radius){
    var volume = radius * radius * height * Math.PI;
    return roundTo2dp(volume);
}


// TODO Write a function which prints the name and volume of a shape, to 2dp.
console.log(getVolume(getRndInteger(1,10),getRndInteger(1,5)));
console.log(getCylinderVolume(getRndInteger(1,10),getRndInteger(1,5)));

// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.
