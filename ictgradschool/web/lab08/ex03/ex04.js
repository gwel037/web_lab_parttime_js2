"use strict";

// FUNCTIONS
// ------------------------------------------

// TODO Complete this function, which should generate and return a random number between a given lower and upper bound (inclusive)
function getRndInteger(min, max) {
    var randomNumber;
    return randomNumber = Math.floor(Math.random() * max) + min;
}

// TODO Complete this function, which should round the given number to 2dp and return the result.
function roundTo2dp(number) {
    return Math.round(number * 100) / 100;
    
}

// TODO Write a function which calculates and returns the volume of a cone.

function getConeVolume(height, radius){
    var volume = Math.PI * (radius * radius) * (height / 3);
    return roundTo2dp(volume);

}
// TODO Write a function which calculates and returns the volume of a cylinder.
function getCylinderVolume(height, radius){
    var volume = radius * radius * height * Math.PI;
    return roundTo2dp(volume);
}


// TODO Write a function which prints the name and volume of a shape, to 2dp.
function getShape(shape,height,radius){
if(shape = "cylinder" || "Cylinder"){
    console.log("The Volume of the Cylinder is " + getCylinderVolume(height,radius));
}else if(shape = "Cone" || "cone"){
    console.log("The Volume of the cone is " + getConeVolume(height,radius));
}else{
    console.log("Please enter the name of a valid shape like Cone or Cylinder")
}
    
    
}
getShape("Cone",2,5);
getShape("Cylinder",2,5);


// ------------------------------------------

// TODO Complete the program as detailed in the handout. You must use all the functions you've written appropriately.
